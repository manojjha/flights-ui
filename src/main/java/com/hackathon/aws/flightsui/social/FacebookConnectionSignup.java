package com.hackathon.aws.flightsui.social;

import com.google.common.collect.Sets;
import com.hackathon.aws.flightsui.dao.PreferenceDao;
import com.hackathon.aws.flightsui.dao.UserDao;
import com.hackathon.aws.flightsui.model.Preference;
import com.hackathon.aws.flightsui.model.User;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@Service
public class FacebookConnectionSignup implements ConnectionSignUp {

    @Autowired
    private UserDao userRepository;

    @Autowired
    private PreferenceDao preferenceDao;

    @Override
    public String execute(Connection<?> connection) {

        User user = userRepository.findByEmailId(connection.getProfileUrl());
        if (user == null) {
            user = new User();
            user.setFirstName(connection.getDisplayName());
            user.setEmailId(connection.getProfileUrl());
            user.setPassword(randomAlphabetic(8));
            user.setPostalCode(randomAlphabetic(4));
            user.setCountry("USA");
            user.setActive(true);
            user.setCreatedOn(DateTime.now());
            user.setUpdatedOn(DateTime.now());
            user.setCreatedBy("app");
            Set<Preference> preferences = Sets.newHashSet(createPreference("Game", "My Game"), createPreference("Music", "My Music"));
            user.setPreferences(preferences);
            userRepository.save(user);
        }
        return user.getEmailId();
    }

    private Preference createPreference(String name, String description) {

        Preference preference = preferenceDao.findByPreferenceNameAndDescription(name, description);
        if (preference == null) {
            preference = new Preference();
            preference.setPreferenceName(name);
            preference.setDescription(description);
        }
        return preference;
    }

}
