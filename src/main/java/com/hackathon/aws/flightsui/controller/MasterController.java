package com.hackathon.aws.flightsui.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hackathon.aws.flightsui.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by ksaleh on 11/28/17.
 */
@RestController
public class MasterController {

    @Autowired
    FlightService flightService;

    @GetMapping("/")
    public ResponseEntity<Map<String, String>> redirect() {
        Map<String, String> response = Maps.newHashMap();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.put(HttpHeaders.LOCATION, Lists.newArrayList("/app/home.html"));
        return new ResponseEntity<>(response, httpHeaders, HttpStatus.MOVED_PERMANENTLY);
    }

    @GetMapping("/flights/search/{from}/{to}")
    public String getFlightSearchResults(@PathVariable("from") String from, @PathVariable("to") String to){
        return flightService.getFlightSearch(from, to);
    }
}
