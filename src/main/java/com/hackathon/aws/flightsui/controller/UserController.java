package com.hackathon.aws.flightsui.controller;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by ksaleh on 11/29/17.
 */
@RestController
@RequestMapping("auth")
public class UserController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/login")
    public ResponseEntity<Map<String, String>> login(@RequestParam("username") String username, @RequestParam("password") String password) {

        try {
            Authentication authentication =
                    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            Map<String, String> responseMap = ImmutableMap.of("username", authentication.getName(),
                    "role", authentication.getAuthorities().stream().findFirst().get().getAuthority());
            return new ResponseEntity<>(responseMap, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> responseMap = ImmutableMap.of("username", "",
                    "role", "ROLE_UNAUTHORIZED");
            return new ResponseEntity<>(responseMap, HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/login")
    public ResponseEntity<Map<String, String>> getLoginStatus() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            Map<String, String> responseMap = ImmutableMap.of("username", "",
                    "role", "ROLE_UNAUTHORIZED");
            return new ResponseEntity<>(responseMap, HttpStatus.UNAUTHORIZED);
        }
        Map<String, String> responseMap = ImmutableMap.of("username", authentication.getName(),
                "role", authentication.getAuthorities().stream().findFirst().get().getAuthority());
        return new ResponseEntity<>(responseMap, HttpStatus.OK);
    }

    @GetMapping(path = "/test")
    public String testGet() {
        return "Hello";
    }

    @GetMapping(path = "/success/logout")
    public ResponseEntity<Map<String, String>> successLogout() {
        Map<String, String> response = ImmutableMap.<String, String>builder().put("user", "loggedOut").build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
