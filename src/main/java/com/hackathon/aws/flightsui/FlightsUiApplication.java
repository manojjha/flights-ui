package com.hackathon.aws.flightsui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlightsUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlightsUiApplication.class, args);
	}

}
