package com.hackathon.aws.flightsui.dao;

import com.hackathon.aws.flightsui.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ksaleh on 11/28/17.
 */
public interface UserDao extends JpaRepository<User, Integer> {

    User findByEmailId(String emailId);
}
