package com.hackathon.aws.flightsui.dao;

import com.hackathon.aws.flightsui.model.Preference;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ksaleh on 12/11/17.
 */
public interface PreferenceDao extends JpaRepository<Preference, Integer> {

    Preference findByPreferenceNameAndDescription(String preferenceName, String description);
}
