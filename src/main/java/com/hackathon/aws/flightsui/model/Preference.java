package com.hackathon.aws.flightsui.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

/**
 * Created by ksaleh on 11/28/17.
 */
@Entity
public class Preference {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String preferenceName;

    private String description;

    @ManyToMany(mappedBy = "preferences")
    private Set<User> users;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPreferenceName() {
        return preferenceName;
    }

    public void setPreferenceName(String preferenceName) {
        this.preferenceName = preferenceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Preference that = (Preference) o;

        if (id != that.id)
            return false;
        if (preferenceName != null ? !preferenceName.equals(that.preferenceName) : that.preferenceName != null)
            return false;
        return description != null ? description.equals(that.description) : that.description == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (preferenceName != null ? preferenceName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Preference{" +
                "id=" + id +
                ", preferenceName='" + preferenceName + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
