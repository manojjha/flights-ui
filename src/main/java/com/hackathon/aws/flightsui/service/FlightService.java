package com.hackathon.aws.flightsui.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by manojjha on 12/21/17.
 */
@Service
public class FlightService {

    @Value("${flight.server}")
    private String flightServer;

    @Value("${flight.search.url}")
    private String flightSearchUrl;

    RestTemplate restTemplate = new RestTemplate();


    public String getFlightSearch(String from, String to){
        return restTemplate.getForEntity(flightServer + flightSearchUrl + from+"/"+to, String.class).getBody();
    }
}
