package com.hackathon.aws.flightsui.service;

import com.hackathon.aws.flightsui.dao.UserDao;
import com.hackathon.aws.flightsui.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;

/**
 * Created by ksaleh on 11/29/17.
 */
@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userDao.findByEmailId(username);

        if (user == null) {
            throw new UsernameNotFoundException("No user found");
        }

        return new org.springframework.security.core.userdetails.User(username, user.getPassword(), true, true, true, true,
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
    }
}
