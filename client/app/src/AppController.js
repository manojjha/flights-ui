/**
 * Main App Controller for the Angular Material Starter App
 * @param UsersDataService
 * @param CookiesService
 * @param $mdSidenav
 * @constructor
 */
function AppController(UsersDataService, CookiesService, $mdSidenav, $log, $http) {
  var self = this;

    self.selected     = null;
    self.users        = [ ];
    self.selectUser   = selectUser;
    self.toggleList   = toggleUsersList;
    self.offers       = [
        {name: "Air India", price:3000.00},
        {name: "Spice Jet", price:4000.00},
        {name: "Jet Airways", price:5000.00},
        {name: "Go Air", price:6000.00}

    ];


  // Load all registered users

  UsersDataService
        .loadAllUsers()
        .then( function( users ) {
          self.users    = [].concat(users);
          self.selected = users[0];
        });

  // *********************************
  // Internal methods
  // *********************************

  /**
   * Hide or Show the 'left' sideNav area
   */
  function toggleUsersList() {
    $mdSidenav('left').toggle();
  }
  /**
   * Select the current avatars
   * @param menuId
   */
  function selectUser ( user ) {
    self.selected = angular.isNumber(user) ? $scope.users[user] : user;
  }


    //Auto-fill Origin/Destination

    self.simulateQuery = false;
    self.isDisabled    = false;

    // list of `state` value/display objects
    self.states        = loadAll();
    self.querySearch   = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange   = searchTextChange;

    self.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? self.states.filter( createFilterFor(query) ) : self.states,
            deferred;
        if (self.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        $log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        $log.info('Item changed to ' + JSON.stringify(item));
    }

    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        // var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,\
        //       Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,\
        //       Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,\
        //       Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,\
        //       North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,\
        //       South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,\
        //       Wisconsin, Wyoming';
        var allStates = 'LAX, JFK';

        return allStates.split(/, +/g).map( function (state) {
            return {
                value: state.toLowerCase(),
                display: state
            };
        });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    };

    //for date pickers
    self.fromDate = null;
    self.toDate = null;

    self.appceptableAdutlPassengers = [1, 2, 3, 4];

    self.executeSearch = function executeSearch(self1) {
        window.location.href='/app/index.html?from=' + self.selectedItemOrigin.display + "&to=" + self.selectedItemDestination.display
        // $log.info(self);
        // $http({
        //     method: 'GET',
        //     url: "/flights/search/" + self.selectedItemOrigin.display + "/" + self.selectedItemDestination.display,
        //     headers: {'Content-Type': 'application/json'},
        //     // transformRequest: function(obj) {
        //     //     var str = [];
        //     //     for(var p in obj)
        //     //         str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        //     //     return str.join("&");
        //     // },
        // }).success(function (response) {
        //     $log.info(response);
        //
        //     self.offers       = [
        //         {name: "Air Ind", price:3000.00},
        //         {name: "Spice Jetty", price:4000.00},
        //         {name: "Jet Airways", price:5000.00},
        //         {name: "Go Air", price:6000.00}
        //
        //     ];
        //      window.location.href='/app/index.html'
        // });

    };


    self.isFormVisible = false;


    self.showForm = function (control) {
        $log.info(control);
        if (self.isFormVisible == true) {
            self.isFormVisible = false;
        } else {
            self.isFormVisible = true;
        }
    };


    self.user = {};
    
    self.signIn = function (control) {
        $log.info(self.user);
        $http({
            method: 'POST',
            url: "/auth/login",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {username: self.user.userName, password: self.user.password}
        }).success(function (response) {
            CookiesService.loadSession(response);
            // window.location.href='/app/home.html'
        });
    };

    self.signOut = function () {
        $http({
            method: 'GET',
            url: "/logout",
        }).success(function (response) {
            CookiesService.clearSession();
            // window.location.href='/app/home.html'
        });
    };

    self.submitRegistrationForm = function (control) {
        $log.info(self.user);
    };

    self.checkLogin = true;
    self.getLoggedInUser = function () {
        var loggedInUser = CookiesService.getLoggedInUser();
        if (self.checkLogin && (loggedInUser == null || loggedInUser == "")) {
            self.checkLogin = false;
            $http({
                method: 'GET',
                url: "/auth/login",
            }).success(function (response) {
                CookiesService.loadSession(response);
                // window.location.href='/app/home.html'
            });
        }
        return CookiesService.getLoggedInUser();
    };





    //search section
    // self.result = searchFlightsOnIndex;

    function searchFlightsOnIndex() {

        var from = (new URL(location)).searchParams.get("from");
        var to = (new URL(location)).searchParams.get("to");

        $log.info("hi");
        $log.info(from);
        $log.info(to);
        $http({
            method: 'GET',
            url: "/flights/search/" + from + "/" + to,
            headers: {'Content-Type': 'application/json'},
            // transformRequest: function(obj) {
            //     var str = [];
            //     for(var p in obj)
            //         str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            //     return str.join("&");
            // },
        }).success(function (response) {
            $log.info(response);

            // var objResponse= JSON.parse(response)
            for(var i=0; i<response.FareInfo.length ;i++){
                if(response.FareInfo[i].LowestFare.AirlineCodes[0] == "SY"){
                    response.FareInfo[i].LowestFare.AirlineCodes[0] = "Sun Country Airlines"
                }
                if(response.FareInfo[i].LowestFare.AirlineCodes[0] == "AA"){
                    response.FareInfo[i].LowestFare.AirlineCodes[0] = "American Airlines"
                }
                if(response.FareInfo[i].LowestFare.AirlineCodes[0] == "VX"){
                    response.FareInfo[i].LowestFare.AirlineCodes[0] = "Virgin America"
                }
                if(response.FareInfo[i].LowestFare.AirlineCodes[0] == "DL"){
                    response.FareInfo[i].LowestFare.AirlineCodes[0] = "Delta Airlines"
                }
                if(response.FareInfo[i].LowestFare.AirlineCodes[0] == "AS"){
                    response.FareInfo[i].LowestFare.AirlineCodes[0] = "Alaska Airlines"
                }
            }

            self.offers = response

        });
    }

    searchFlightsOnIndex();

    // return function airlineCodeToName(airCode) {
    //     $log.info("--------------");
    //     $log.info(airCode);
    //     var airName = "unknown"
    //     if (airCode == "SY") {
    //         airName = airCode
    //     }
    //     return airName
    // };


}

export default [ 'UsersDataService', 'CookiesService', '$mdSidenav', '$log', '$http', AppController ];
