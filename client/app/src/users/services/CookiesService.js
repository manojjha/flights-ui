function CookiesService($cookies, $log) {
    var self = this;
    var sessionObj = {};

    self.loadSession = function (response) {
        sessionObj.USER_NAME = response.username;
        sessionObj.ROLE = response.role;
        $cookies.put("user", JSON.stringify(sessionObj));
    };

    self.clearSession = function () {
        sessionObj = {};
        $cookies.put("user", null);
    };

    self.getLoggedInUser = function () {
        if (sessionObj != null && sessionObj.USER_NAME != null && sessionObj.USER_NAME != ""
            && sessionObj.ROLE != null && sessionObj.ROLE != ""
            && sessionObj.USER_NAME != "anonymousUser" && sessionObj.ROLE != "ROLE_ANONYMOUS") {
            return sessionObj.USER_NAME;
        } else {
            var cookieSession = $cookies.get("user");
            if (cookieSession != null && cookieSession != "") {
                cookieSession = JSON.parse(cookieSession);
                if (cookieSession!= null && cookieSession.USER_NAME != null && cookieSession.USER_NAME != ""
                    && cookieSession.ROLE != null && cookieSession.ROLE != ""
                    && cookieSession.USER_NAME != "anonymousUser" && cookieSession.ROLE != "ROLE_ANONYMOUS") {
                    return cookieSession.USER_NAME;
                }
            }
        }
        return "";
    }
}

export default ['$cookies', '$log', CookiesService];
