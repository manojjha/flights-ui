// Notice that we do not have a controller since this component does not
// have any specialized logic.

export default {
    name : 'offersList',
    config : {
        bindings         : { offers: '<' },
        templateUrl      : 'src/users/components/offers/OffersList.html'
    }
};
