// Load the custom app ES6 modules

import UsersDataService from 'src/users/services/UsersDataService';
import CookiesService from 'src/users/services/CookiesService';

import UsersList from 'src/users/components/list/UsersList';
import UserDetails from 'src/users/components/details/UserDetails';

// Define the Angular 'users' module

export default angular
  .module("users", ['ngMaterial', 'ngCookies'])

  .component(UsersList.name, UsersList.config)
  .component(UserDetails.name, UserDetails.config)

  .service("UsersDataService", UsersDataService)
  .service("CookiesService", CookiesService);
